const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoClient = require("mongodb").MongoClient;
const url = process.env.MONGO_URL || "mongodb://mongo:27017";

const ObjectId = require("mongodb").ObjectID;
const fileUpload = require("express-fileupload");
const path = require("path");
const uniqueString = require("unique-string-generator");
const ObsClient = require('esdk-obs-nodejs');
const cron = require("node-cron");

const port = process.env.PORT || 80;
const cloudMessaging = require("./components/cloud_messaging");
const lineFunction = require("./components/line_function");

cron.schedule('*/30 * * * * *', () => {
  let getTime = new Date();
  let day = getTime.getDay();
  let hours = getTime.getHours() + 7;
  let minutes = getTime.getMinutes() + 1;

  console.log("job");
  
  if(minutes >= 60){
    minutes %= 60;
    hours++;
    if(hours >= 24){
      hours %= 24;
      day++;
      day %= 7;
    }
  }
  if(hours >= 24){
    hours %= 24;
    day++;
  }

  day += "";
  if(hours < 10) hours = "0" + hours;
  else hours += "";
  if(minutes < 10) minutes = "0" + minutes;
  else minutes += "";

  mongoClient.connect(url, { useUnifiedTopology: true }, (err, db)=>{
    if(err) throw err;
    const dbcon = db.db("notification");
    dbcon.collection("is_notic").findOne({}, (err, notic)=>{
      if(notic.status){
        dbcon.collection("program").findOne({day, hours, minutes}, (err, programRes)=>{
          if(err) throw err;
          if(programRes){
            dbcon.collection("deviceId").find({}).toArray((err, result)=>{
              if(err) throw err;
              let arr = [];
              result.forEach((item, index)=>{
                arr.push(item.device_id);
                if(index == result.length - 1){
                  cloudMessaging(arr, programRes.name, programRes.img);
                }
              })
            })
          }
        })
      }
    })
  })
})

app
  .use(cors({origin: true}))
  .use(bodyParser.urlencoded({limit: '50mb', extended: true}))
  .use(bodyParser.json({limit: '50mb'}))
  .use(express.json({limit: '50mb'}))
  .use(express.urlencoded({limit: '50mb', extended: true}))
  .use(fileUpload({
    useTempFiles : true,
    tempFileDir : path.join(__dirname,'/tmp'),
  }))
  .use(express.static(path.join(__dirname + "/pages/script")))
  .use(express.static(path.join(__dirname + "/pages/style")))
  .use(express.static(path.join(__dirname + "/pages")))

  //ui
  .get("/", (req, res)=>{
    res.sendFile(__dirname + "/pages/index.html");
  })
  .get("/insert-program-page", (req, res)=>{
    res.sendFile(__dirname + "/pages/insert_program.html");
  })
  .get("/update-program-page", (req, res)=>{
    res.sendFile(__dirname + "/pages/update_program.html");
  })
  .get("/delete-program-page", (req, res)=>{
    res.sendFile(__dirname + "/pages/delete_program.html");
  })
  .get("/clone-job-page", (req, res)=>{
    res.sendFile(__dirname + "/pages/clone_job.html");
  })
  .get("/list-program-page", (req, res)=>{
    res.sendFile(__dirname + "/pages/list_program.html");
  })
  .get("/list-user-page", (req, res)=>{
    res.sendFile(__dirname + "/pages/listuser.html");
  })

  .get("/listuser", (req, res)=>{
    mongoClient.connect(url, (err, db)=>{
      if(err) throw err;
      const dbcon = db.db("notification");

      dbcon.collection("users").find({}).toArray((err, users)=>{
        res.json({users})
      })
    })
  })
  .post("/news", (req, res)=>{
    const { title, detail, option, userId } = req.body
    dataInsert = {
	    title, detail, option, userId
    }

    mongoClient.connect(url, (err, db)=>{
      if(err) throw err;
      const dbcon = db.db("notification");
      dbcon.collection('news').insert(dataInsert, (err, insertedRes)=>{
        if(err) throw err;
        function findToken(filter){
          dbcon.collection("deviceId").find(filter).toArray((err, result)=>{
            if(err) throw err;
            let arr = [];
            result.forEach((item, index)=>{
              arr.push(item.device_id);
              if(index == result.length - 1){
                cloudMessaging(arr, title);
                res.json({status: "success"});
              }
            })
          })
        }
        
        if(userId.length > 0){
          let arrObj = [];
          userId.forEach((item, index)=>{
            arrObj.push(ObjectId(item));
            if(index == userId.length-1){
              filter = { _id: { $in: arrObj } };
              findToken(filter)
            }
          })
        } else findToken({})
        //"profile.name": name
      })
    })
  })
  .post("/insert-program", (req, res)=>{
    const { day, hours, minutes, name } = req.body;

    var obsClient = new ObsClient({
      access_key_id: 'UTPJ3T65IFVEULDVLMGO',
      secret_access_key: 'YRFqIyvRUSw2kPk4czAwrHPW6G0LRn8b9mghox0Q',
      server : 'obs.ap-southeast-2.myhuaweicloud.com'
    });

    if(!day || !hours || !minutes || !name || !req.files) res.status(500).end();
    else{
      mongoClient.connect(url, { useUnifiedTopology: true }, (err, db)=>{
        if (err) throw err;
        const dbcon = db.db("notification");
        dbcon.collection("program").findOne({day, hours, minutes}, (err, result)=>{
          if(err) throw err;
          if(result) res.status(500).end();
          else{
            const targetFile = req.files.img;
            const fileName = uniqueString.UniqueString() + targetFile.name;
            targetFile.mv(path.join(__dirname, '/images', fileName), (err) => {
              if(err) throw err;

              obsClient.putObject({
                Bucket : 'mas',
                Key : fileName,
                /* Body : req.files.img.data, */
                SourceFile: __dirname + `/images/${fileName}`
              }, (err, result) => {
                    if(err) throw err;
                    const dataInsert = {
                      day, hours, minutes, name, img: fileName
                    }
                    dbcon.collection("program").insertOne(dataInsert, (err, doc)=>{
                      if(err) throw err;
                      console.log("success");
                      res.status(200).end();
                    })
              });
              obsClient.close();
            });
          }
        });
      })
    }
  })
  .post("/update-program", (req, res)=>{
    const { day, hours, minutes, cHours, cMinutes, name } = req.body;
    if(!day || !hours || !minutes) res.status(500).end();
    else{
      mongoClient.connect(url, { useUnifiedTopology: true }, (err, db)=>{
        if(err) throw err;
        const dbcon = db.db("notification");
        dbcon.collection("program").findOne({day, hours, minutes}, (err, result)=>{
          if(err) throw err;

          let fileName = "";
          if(!result) res.status(500);
          else{
            if(req.files){
              const targetFile = req.files.img;
              fileName = uniqueString.UniqueString() + targetFile.name;
              targetFile.mv(path.join(__dirname, '/images', fileName), (err) => {
                if (err) throw err;
                update(1);
              });
            } else update(0);
            
            function update(){
              let dataUpdate = {
                $set: {
                  name,
                  hours: cHours,
                  minutes: cMinutes,
                  img: fileName
                }
              }
              if(!name) delete dataUpdate.$set.name;
              if(!cHours || !cMinutes){
                delete dataUpdate.$set.hours;
                delete dataUpdate.$set.minutes;
              }
              if(!req.files) delete dataUpdate.$set.img;
              console.log(dataUpdate);
              dbcon.collection("program").updateOne({day, hours, minutes}, dataUpdate, (err, doc)=>{
                if(err) throw err;
                res.json({status: "success"});
              })
            }
          }
        })
      })
      
    }
  })
  .get("/list-program-all", (req, res)=>{
    console.log("here")
    mongoClient.connect(url, { useUnifiedTopology: true }, (err, db)=>{
      if(err) throw err;
      const dbcon = db.db("notification");
      dbcon.collection("program").find({}).toArray((err, result)=>{
        if(err) throw err;
        
        function asyncSort(){
          return new Promise((resolve, reject)=>{
            const sorted = result.sort( (a, b)=>{
              a = parseInt(a.hours*60 + a.minutes);
              b = parseInt(b.hours*60 + b.minutes)
              if (a < b) return -1;
              if (a > b) return 1;
              return 0;
            });
            resolve(sorted);
          })
        }
        asyncSort().then(sorted=>{
          dbcon.collection("is_notic").findOne({}, (err, notic)=>{
            if(err) throw err;
            res.json({list: sorted, status: notic.status});
          })
        })
      })
    })
  })
  .post("/delete-program", (req, res)=>{
    const { day, hours, minutes } = req.body;
    mongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db)=>{
      if(err) throw err;
      const dbcon = db.db("notification");
      dbcon.collection("program").deleteOne({day, hours, minutes}, (err, doc)=>{
        if(err) throw err;
        res.status(200).json({status: "success"});
      })
    })
  })
  .post("/select-program", (req, res)=>{
    const { day, hours, minutes } = req.body;
    mongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db)=>{
      if(err) throw err;
      const dbcon = db.db("notification");
      dbcon.collection("program").findOne({day, hours, minutes}, (err, result)=>{
        if(err) throw err;
        if(result) res.json({program: result});
        else res.status(500).end();
      })
    })
  })
  .put("/set-notic", (req, res)=>{
    const { status } = req.body;
    mongoClient.connect(url, {useUnifiedTopology: true},(err, db)=>{
      if(err) throw err;
      const dbcon = db.db("notification");
      dbcon.collection("is_notic").updateMany({}, {$set: {status}}, (err, doc)=>{
        if(err) throw err;
        console.log("updated");
        res.json({status: "success"});
      })
    })
  })

  .post("/profile", (req, res)=>{
    const {
      // --- profile
      email,
      phoneNumber,
      name,
      // --- profile
      socialAccount,
    } = req.body;

    mongoClient.connect(url, (err, db)=>{
      if (err) throw err;

      usersObj = {
        profile: {
          email,
          phoneNumber,
          name
        },
        socialAccount
      }
      const dbcon = db.db("notification");
      dbcon.collection("users").insertOne(usersObj, (err, resInserted)=>{
        if(err) throw err
        console.log("Inserted Data!")
        db.close();
        res.end()
      })
    })
  })
  .post("/setdevice", (req, res)=>{
    const { platform_type, device_id } = req.body;

    if(!platform_type || !device_id) res.json({status: "failed", msg: "data not valid"});
    else{
      mongoClient.connect(url, { useUnifiedTopology: true }, (err, db)=>{
        if(err) throw err;
        const dbcon = db.db("notification");
        dbcon.collection("deviceId").findOne({device_id}, (err, doc)=>{
          if(err) throw err;
          if(!doc){
            dbcon.collection("deviceId").insertOne({platform_type, device_id}, (err, doc)=>{
              if(err) throw err;
              res.json({status: "success", msg: "inserted data."});
            })
          } else res.json({status: "failed", msg: "Token has stored."});
        })
      })
    }
  })
  .post("/line", (req, res, next)=>{
    lineFunction(req.body).then(token=>{
      res.end(token);
    }).catch(next)
  })
  .listen(port, ()=>console.log(`> App on port ${port}`))
