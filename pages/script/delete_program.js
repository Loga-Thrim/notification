function programName(){
    const day = document.getElementById("day").value;
    let time = document.getElementById("time").value;

    if(day && time){
        time = time.split(":");
        const hours = time[0];
        const minutes = time[1];

        document.getElementById("loader").style.display = "inline-block";
        document.getElementById("name").style.display = "none";

        fetch("https://noti.topnews.co.th/select-program", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                day, hours, minutes
            })
        }).then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res){
                document.getElementById("name").style.display = "inline-block";
                document.getElementById("name").innerHTML = res.program.name;
                document.getElementById("loader").style.display = "none";
            }
        })
        .catch(e=>{
            document.getElementById("name").style.display = "inline-block";
            document.getElementById("loader").style.display = "none";
        })
    }
}

function submit(){
    const day = document.getElementById("day").value;
    let time = document.getElementById("time").value.split(":");
    const hours = time[0];
    const minutes = time[1];

    document.getElementById("loader2").style.display = "block";

    console.log(day, hours, minutes)

    if(document.getElementById("name").textContent == "-") {
        alert("ระบุรายการให้ถูกต้อง");
        document.getElementById("loader2").style.display = "none";
    }
    else{
        fetch("https://noti.topnews.co.th/delete-program", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                day, hours, minutes
            })
        }).then(res=>res.json())
        .then(res=>{
            if(res.status === "success"){
                alert("ลบรายการสำเร็จ");
                window.location.reload();
            } else alert("เกิดข้อผิดพลาด ลองใหม่อีกครั้งภายหลัง");
            document.getElementById("loader2").style.display = "none";
        })
    }
}