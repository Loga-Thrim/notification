function upload(){
    const day = document.getElementById("day").value;
    const time = document.getElementById("time").value.split(":");
    const hours = time[0];
    const minutes = time[1];
    const name = document.getElementById("name").value;
    const img = document.getElementById("img").files[0];

    document.getElementById("loader2").style.display = "block";

    if(/* !day || !hours || !minutes || !name || !img */0) alert("กรอกข้อมูลให้ครบถ้วน");
    else{
        const formData = new FormData();
        formData.append("day", day);
        formData.append("hours", hours);
        formData.append("minutes", minutes);
        formData.append("name", name);
        formData.append("img", img);

        console.log(formData)

        fetch("https://noti.topnews.co.th/insert-program", {
            method: "POST",
            body: formData
        }).then(res=>res.json())
        .then(res=>{
            alert("เพิ่มข้อมูลสำเร็จ");
            window.location.reload();
        })
        .catch(e=>{
            alert("เพิ่มข้อมูลสำเร็จ");
            window.location.reload();
            /*alert("เพิ่มข้อมูลล้มเหลว ลองใหม่อีกครั้งภายหลัง"); */
        })
    }
}