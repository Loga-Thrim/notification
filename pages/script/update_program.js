function programSelect(){
    const day = document.getElementById("day").value;
    let time = document.getElementById("time").value;

    if(day && time){
        time = time.split(":");
        const hours = time[0];
        const minutes = time[1];

        /* document.getElementById("loader").style.display = "inline-block";
        document.getElementById("name").style.display = "none"; */
        document.getElementById("loader2").style.display = "block";

        fetch("https://noti.topnews.co.th/select-program", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                day, hours, minutes
            })
        }).then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res){
                document.getElementById("cTime").value = res.program.hours + ":" + res.program.minutes;
                document.getElementById("name").value = res.program.name;
                document.getElementById("imgShow").innerHTML = 
                `<img src='https://mas.topnews.co.th/${res.program.img}' width=200></img>`;
                document.getElementById("loader2").style.display = "none";
                /* document.getElementById("name").style.display = "inline-block";
                document.getElementById("name").innerHTML = res.program.name;
                document.getElementById("loader").style.display = "none"; */
            }
        })
        .catch(e=>{
            /* document.getElementById("name").style.display = "inline-block";
            document.getElementById("loader").style.display = "none"; */
            document.getElementById("loader2").style.display = "none";
        })
    }
}

function upload(){
    const day = document.getElementById("day").value;
    const time = document.getElementById("time").value.split(":");
    const cTime = document.getElementById("cTime").value.split(":");
    const hours = time[0];
    const minutes = time[1];
    const cHours = cTime[0];
    const cMinutes = cTime[1];
    const name = document.getElementById("name").value;
    const img = document.getElementById("img").files[0];

    document.getElementById("loader2").style.display = "block";

    if(!day || !hours || !minutes) alert("ระบุวันและเวลาเดิมของรายการที่จะอัพเดท");
    else{
        const formData = new FormData();
        formData.append("day", day);
        formData.append("hours", hours);
        formData.append("minutes", minutes);
        formData.append("cHours", cHours);
        formData.append("cMinutes", cMinutes);
        formData.append("name", name);
        formData.append("img", img);

        fetch("https://noti.topnews.co.th/update-program", {
            method: "POST",
            body: formData
        }).then(res=>res.json())
        .then(res=>{
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location.reload();
        })
        .catch(e=>{
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location.reload();
            /* alert("แก้ไขข้อมูลล้มเหลว ลองใหม่อีกครั้งภายหลัง"); */
        })
    }
}