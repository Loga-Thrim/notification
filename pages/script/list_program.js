const loader = document.getElementById("loader2");

function submitNotic(status){
    loader.style.display = "block";

    fetch("https://noti.topnews.co.th/set-notic", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({status})
    }).then(res=>res.json())
    .then(res=>{
        window.location.reload();
    })
    .catch(e=>{
        window.location.reload();
    })
}

function init(){
    fetch("https://noti.topnews.co.th/list-program-all").then(res=>res.json())
    .then(res=>{
        const status =  document.getElementById("status");
        if(res.status) 
            status.innerHTML = `<button class="btn-notic-f"
                                onclick="submitNotic(0)">ปิดการแจ้งเตือน</button>`;
        else 
            status.innerHTML = `<button class="btn-notic-t"
                                onclick="submitNotic(1)">เปิดการแจ้งเตือน</button>`;

        let dom = "";
        res.list.forEach((item, index)=>{
            let time = item.hours + ":" + item.minutes;
            dom += `<tr>
                <th>${time}</th>`;

            for(let i=1;i<=7;++i){
                if(i == item.day) dom += `<th>${item.name}</th>`;
                else dom += "<th></th>";
            }
            dom += "</tr>";

            if(index == res.list.length - 1){
                document.getElementById("body-time").innerHTML = dom;
            }
        })

        loader.style.display = "none";
    })
}
init();
