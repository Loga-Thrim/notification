function sendData(){
    let title = document.getElementById("title").value;
    let detail = document.getElementById("detail").value;
    let option = document.getElementById("option").value;
    let userId = document.getElementById("userid").value.split(/ |\n/)
    let user = document.getElementById("user").value;

    if(user == "all" || userId[0] == "") userId = []
    dataObj = {
        title,
        detail,
        option,
        userId
    }
    //
    fetch("https://noti.topnews.co.th/news", {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(dataObj)
    })
}

function selected(){
    let user = document.getElementById("user").value;
    let inputUserID = document.getElementById("inputUserId");
    if(user == "some") inputUserID.style.display = "unset";
    else inputUserID.style.display = "none";
}