const admin = require('firebase-admin')
const functions = require('firebase-functions')
const axios = require('axios')

const serviceAccount = require('../serviceaccount.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

async function verifyLineToken (body) {
  try {
    const { data } = await axios.get('https://api.line.me/oauth2/v2.1/verify', {
      params: {
        access_token: body.accessToken
      }
    })

    if (data.client_id != "1655702697") {
      throw new Error('LINE channel ID mismatched')
    }
    return await getFirebaseUser(body)
  } catch (error) {
    throw new Error(error)
  }
}

async function getFirebaseUser (body) {
  const firebaseUid = `line:${body.uid}`
  let userRecord
  try {
    userRecord = await admin.auth()
      .getUser(firebaseUid)
  } catch (error) {
    if (error.code === 'auth/user-not-found') {
      const userData = {
        uid: firebaseUid,
        displayName: body.displayName,
        photoURL: body.photoURL || 'https://firebasestorage.googleapis.com/v0/b/top-news-6b7c7.appspot.com/o/profile.jpg?alt=media&token=f536a2a7-1604-4421-9069-61713a085c51'
      }
      if (body.email) {
        userData.email = body.email
      }
      userRecord = await admin.auth().createUser(userData)
    } else {
      throw error
    }
  }
  return await admin.auth().createCustomToken(userRecord.uid)
}

/* const app = express()

app.use(cors({ origin: true })) */

// build multiple CRUD interfaces:
/* app.post('/line', (req, res, next) => {
  verifyLineToken(req.body).then(token => {
    res.send(token)
  }).catch(next)
}) */

module.exports = (body)=>{
  return new Promise((resolve, reject)=>{
    verifyLineToken(body).then(token=>resolve(token))
    .catch(e=>{
      reject(e);
    })
  })
}
