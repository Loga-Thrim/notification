# Endpoint "https://noti.topnews.co.th"
## Add profile path "/profile" with post method.
### headers
    "Content-Type": "application/json"
### body
    "email"
    "name"
    "phoneNumber"
    "socialAccount"
## Add device id path "/setdevice" with post method.
### headers
    "Content-Type": "application/json"
### body
    "platform_type"
    "device_id"
## Line authentication path "/line" with post method.
### headers
    "Content-Type": "application/json"
### body
    <unknow>
